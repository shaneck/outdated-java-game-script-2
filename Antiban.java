package sevenQP;

import org.tbot.methods.Mouse;
import org.tbot.methods.Random;
import org.tbot.methods.Time;
import org.tbot.methods.Widgets;
import org.tbot.methods.input.keyboard.Keyboard;
import org.tbot.methods.tabs.Inventory;
import org.tbot.wrappers.WidgetChild;

import java.awt.*;

public abstract class Antiban{

    public static void loseFocus(){
        if (Mouse.move(-111, Random.nextInt(1, 350))){
            Time.sleep(100, 800);
            Keyboard.loseFocus();
        }
    }

    public static void requestFocus(){
        Mouse.setLocation(Random.nextInt(25, 519), Random.nextInt(28, 455));
        Time.sleep(100, 400);
        Keyboard.requestFocus();
    }

    public static void hoverRandomSkill(){
        WidgetChild skills = Widgets.getWidget(548, 43);
        WidgetChild useChild = Widgets.getWidget(320, Random.nextInt(1, 23));
        if (skills.getTextureID() == 1030){
            Mouse.move(useChild.getX() + Random.nextInt(10, 55), useChild.getY() + Random.nextInt(5, 25));
            Time.sleep(1600, 2100);
            Inventory.openTab();
        }else{
            int chance = Random.nextInt(1, 2);
            skills.click();
            Time.sleep(50,  180);
            Mouse.move(useChild.getX() + Random.nextInt(10, 55), useChild.getY() + Random.nextInt(5, 25));
            Time.sleep(1600, 2100);
            if (chance == 2){
                WidgetChild useC = Widgets.getWidget(320, Random.nextInt(1, 23));
                Mouse.move(useC.getX() + Random.nextInt(10, 55), useC.getY() + Random.nextInt(5, 25));
                Time.sleep(1300, 1500);
            }
            Inventory.openTab();
        }
    }

    public static void moveMouseInventory(){
        Rectangle inventorySlot = Inventory.getBounds(Random.nextInt(1, 26));
        if (inventorySlot != null){
            Mouse.moveMouse(inventorySlot.x, inventorySlot.y, Random.nextInt(-10, 10), Random.nextInt(-10, 10));
        }
    }

    public static void moveMouseRandomly(){
        Mouse.moveRandomly();
    }

}