package sevenQP;

import org.tbot.bot.TBot;
import org.tbot.methods.web.Web;
import org.tbot.methods.web.actions.ObjectAction;
import org.tbot.methods.web.areas.WebArea;
import org.tbot.methods.web.nodes.WebNode;
import org.tbot.methods.web.nodes.connections.ActionConnection;
import org.tbot.wrappers.Tile;

/**
 * Created by Shaneee on 26/06/2015.
 */
public class webArea extends WebArea{
    @Override
    public void reset(Web web){

    }

    @Override
    public void addTo(Web web){
        WebNode startNode = web.getNearestWebNode(new Tile(3165,3433,0));
        WebNode node0 = web.getNode(new Tile(3159,3436,0));
        WebNode node1 = web.getNode(new Tile(3155,3436,1));
        WebNode node2 = web.getNode(new Tile(3158,3426,1));
        web.addWalkConnection(startNode, node0);
        ObjectAction ObjectAction3 = new ObjectAction(new Tile(3156,3436,1), "Staircase", "Climb-down");
        ObjectAction ObjectAction4 = new ObjectAction(new Tile(3158,3436,0), "Staircase", "Climb-up");
        ActionConnection nodeConnector1 = new ActionConnection(node0, node1, ObjectAction4);
        ActionConnection nodeConnector2 = new ActionConnection(node1, node0, ObjectAction3);
        node0.addConnection(nodeConnector1);
        node1.addConnection(nodeConnector2);
        web.addWalkConnection(node1, node2);
        web.addWalkConnection(node2, node1);

    }

}
