package sevenQP;

import critCrafter.methods;
import org.tbot.bot.TBot;
import org.tbot.internal.AbstractScript;
import org.tbot.internal.Manifest;
import org.tbot.internal.handlers.LogHandler;
import org.tbot.methods.*;
import org.tbot.methods.ge.GrandExchange;
import org.tbot.methods.tabs.Inventory;
import org.tbot.methods.input.keyboard.Keyboard;
import org.tbot.methods.walking.Walking;
import org.tbot.methods.web.Web;
import org.tbot.methods.web.path.WebPath;
import org.tbot.util.botcontrol.BotControlConnection;
import org.tbot.wrappers.*;

/**
 * Created by Shaneee on 24/06/2015.
 */

@Manifest(name = "sevenQP", authors = "Crithane", version = 0.1)
public class mainHandler extends AbstractScript {
    public boolean COMPLETINGQUEST;
    private sevenQP.Timer t = new sevenQP.Timer();
    int t2 = Random.nextInt(5,15);
    int graininHop = 0;
    public boolean spin;
    int inSpin;
    int stileClimbed;
    Tile sheep = new Tile(3202, 3267, 0);
    Area romeo = new Area(new Tile[]  {new Tile(3223,3437,0),new Tile(3221,3421,0),new Tile(3207,3421,0),new Tile(3206,3437,0)});
    Tile fountain = new Tile(3213, 3429, 0);
    Web web = new Web();
    BotControlConnection bc;


    @Override
    public boolean onStart(){

        web.getData().add(new webArea());
        return true;
    }

    @Override
    public int loop() {
        if(Game.isLoggedIn()) {
            int CASETTING = Settings.get(29);
            int QPSETTING = Settings.get(101);
            int MILLSETTING = Settings.get(695);
            int SHEEPSETTING = Settings.get(179);
            int ROMEOSETTING = Settings.get(144);
            Tile cook = new Tile(3209, 3213, 0);
            Mouse.setSpeed(Random.nextInt(30, 35));
            Antiban();
            WidgetChild questclose = Widgets.getWidget(277, 15);
//        if (Inventory.getCount("Wool") >= 20 || Inventory.getCount("Wool") + Inventory.getCount("Ball of Wool") >= 20){
//            spin = true;
//        }
            if (Inventory.getCount("Wool") >= 20) {
                spin = true;
            }

            if (questclose != null && questclose.isOnScreen()) {
                questclose.click();
            }
            switch (QPSETTING) {
                case 0:
                    switch (CASETTING) {
                        case 0:
                                if (cook != null && cook.distance() > 5) {
                                    methods.walkTo(cook, 5);
                                } else {
                                    if (!NPCChat.isChatOpen()) {
                                        methods.talkTo("Cook");
                                        methods.dynamicSleep(CASETTING == 1, 600, 800);
                                    } else if (NPCChat.canContinue()) {
                                        methods.handleContinue();
                                        methods.dynamicSleep(!NPCChat.canContinue(), 600, 800);
                                    } else if (NPCChat.isChatOptionsOpen()) {
                                        NPCChat.selectOption("What's wrong?");
                                        methods.dynamicSleep(NPCChat.isChatOptionsOpen(), 600, 800);
                                        NPCChat.selectOption("I'm always happy to help a cook in distress.");
                                        methods.dynamicSleep(NPCChat.isChatOptionsOpen(), 600, 800);
                                    }
                                }
                            //need to start quest
                            break;
                        case 1:
                            if (!potCollected() && !eggCollected() && !milkCollected() && !flourCollected() && COMPLETINGQUEST != true) {
                                if (cook != null && cook.distance() > 5) {
                                    methods.walkTo(cook, 5);
                                } else {
                                    GroundItem pot = GroundItems.getNearest("Pot");
                                    if (pot != null && pot.isOnScreen()) {
                                        pot.pickUp();
                                        methods.dynamicSleep(Inventory.contains("Pot"), 600, 800);
                                    } else if (pot != null && !pot.isOnScreen()) {
                                        Camera.turnTo(pot);
                                        methods.dynamicSleep(pot.isOnScreen(), 600, 800);
                                    } else {
                                        methods.dynamicSleep(pot != null, 600, 800);
                                    }
                                }
                            } else if (potCollected() && !eggCollected() && !milkCollected() && !flourCollected() && COMPLETINGQUEST != true || !bucketCollected() && !milkCollected() && COMPLETINGQUEST != true || !potCollected() && !flourCollected() && COMPLETINGQUEST != true) {
                                if (!bucketCollected()) {
                                    Tile buckettile = new Tile(3225, 3292, 0);
                                    GroundItem bucket = GroundItems.getNearest("Bucket");
                                    if (buckettile != null && buckettile.distance() > 3) {
                                        methods.walkTo(buckettile, 3);
                                    } else if (buckettile != null && buckettile.distance() < 3 && bucket != null) {
                                        bucket.pickUp();
                                        methods.dynamicSleep(bucketCollected(), 600, 800);
                                    }
                                } else {
                                    Tile eggTile = new Tile(3229, 3298, 0);
                                    if (eggTile != null && eggTile.distance() > 5) {
                                        methods.walkTo(eggTile, 5);
                                    } else if (bucketCollected() && eggTile.distance() < 5) {
                                        GroundItem egg = GroundItems.getNearest("Egg");
                                        if (egg != null && egg.isOnScreen()) {
                                            egg.pickUp();
                                            methods.dynamicSleep(Inventory.contains("Egg"), 600, 800);
                                        } else if (egg != null && !egg.isOnScreen()) {
                                            Camera.turnTo(egg);
                                            methods.dynamicSleep(egg.isOnScreen(), 600, 800);
                                        } else {
                                            methods.dynamicSleep(egg != null, 600, 800);
                                        }
                                    }
                                }
                            } else if (bucketCollected() && eggCollected() && !milkCollected() && COMPLETINGQUEST != true) {
                                Tile cow = new Tile(3255, 3275, 0);
                                GameObject dairy = GameObjects.getNearest("Dairy cow");
                                if (cow != null && cow.distance() > 5) {
                                    methods.walkTo(cow, 5);
                                } else if (cow != null && cow.distance() < 5) {
                                    if (dairy != null && dairy.isOnScreen()) {
                                        dairy.interact("Milk");
                                        methods.dynamicSleep(milkCollected(), 1500, 2000);
                                    } else if (dairy != null && !dairy.isOnScreen()) {
                                        Camera.turnTo(dairy);
                                        methods.dynamicSleep(dairy.isOnScreen(), 600, 800);
                                    }
                                }

                            } else if (potCollected() && !flourCollected() && eggCollected() && milkCollected() && COMPLETINGQUEST != true) {
                                GameObject hopper = GameObjects.getNearest("Hopper");
                                if (!wheatCollected() && !atMill() && !hopGrain() && COMPLETINGQUEST != true || !wheatCollected() && !hopperFlour() && COMPLETINGQUEST != true && !hopGrain()) {
                                    Tile wheatTile = new Tile(3162, 3291, 0);
                                    if (wheatTile != null && wheatTile.distance() > 2) {
                                        methods.walkTo(wheatTile, 2);
                                    } else {
                                        GameObject wheat = GameObjects.getNearest("Wheat");
                                        if (wheat != null) {
                                            wheat.interact("Pick");
                                            methods.dynamicSleep(Inventory.contains("Grain"), 600, 800);
                                        }
                                    }
                                } else {
                                    GameObject door = GameObjects.getNearest("Large door");
                                    Tile mill = new Tile(3167, 3305, 0);
                                    Tile millMiddle = new Tile(3167, 3305, 1);
                                    Tile millTop = new Tile(3165, 3307, 2);
                                    GameObject ladder = GameObjects.getNearest("Ladder");
                                    GameObject hopperControls = GameObjects.getNearest("Hopper controls");
                                    GameObject flourBin = GameObjects.getNearest("Flour bin");
                                    if (!hopperFlour() && !atMill() && !hopGrain()) {
                                        methods.walkTo(mill, 5);
                                    } else if (mill.isOnScreen() && hopper == null && !hopperFlour() && !hopGrain()) {
                                        if (door != null) {
                                            if (door.getActions().toString().contains("Open")) {
                                                door.interact("Open");
                                                methods.dynamicSleep(!door.getActions().toString().contains("Open"), 600, 800);
                                            }
                                        }
                                        if (ladder != null) {
                                            ladder.interact("Climb-up");
                                            methods.dynamicSleep(!mill.isOnScreen(), 1000, 1200);
                                        }

                                    } else if (millMiddle.isOnScreen() && hopper == null && !hopperFlour() && !hopGrain()) {
                                        ladder.interact("Climb-up");
                                        methods.dynamicSleep(!millMiddle.isOnScreen(), 600, 800);
                                    } else if (hopper != null && !hopperFlour() || hopGrain() && !hopperFlour()) {
                                        if(!hopGrain()) {
                                            Inventory.useItemOn("Grain", hopper);
                                            sleep(3000, 4000);
                                            graininHop = 1;
                                        }
                                        if (hopperControls != null) {
                                            if (hopGrain()) {
                                                hopperControls.interact("Operate");
                                                methods.dynamicSleep(MILLSETTING == 1, 2000, 2500);
                                            }
                                        }
                                    } else if (hopperFlour() && atMill() && flourBin == null) {
                                        ladder.interact("Climb-down");
                                        methods.dynamicSleep(!millMiddle.isOnScreen(), 600, 800);
                                    } else if (hopperFlour() && flourBin != null) {
                                        flourBin.interact("Empty");
                                        methods.dynamicSleep(flourCollected(), 600, 800);
                                    }
                                }
                            } else if (flourCollected() && milkCollected() && eggCollected() || COMPLETINGQUEST == true) {
                                COMPLETINGQUEST = true;
                                if (cook.distance() > 5) {
                                    methods.walkTo(cook, 5);
                                } else {
                                    if (!NPCChat.isChatOpen()) {
                                        methods.talkTo("Cook");
                                        methods.dynamicSleep(CASETTING == 3, 600, 800);
                                    } else if (NPCChat.canContinue()) {
                                        methods.handleContinue();
                                        methods.dynamicSleep(!NPCChat.canContinue(), 600, 800);
                                    }
                                }
                            }
                            //quest started
                            //do shit
                            break;
                        case 2:
                            //quest finished
                            //don't do shit
                            break;
                    }
//                do cooks assitant quest
                    break;

                case 1:
                    switch (SHEEPSETTING) {
                        case 0:
                            Tile sheepQuest = new Tile(3190, 3272, 0);
                            NPC fred = Npcs.getNearest("Fred the Farmer");
                            if (Inventory.getCount("Wool") >= 20 || Inventory.getCount("Wool") + Inventory.getCount("Ball of Wool") >= 20) {
                                spin = true;
                            }
                            if (needToBank(28)) {
                                if (!Bank.isOpen()) {
                                    LogHandler.log("Want to bank");
                                    Bank.openNearestBank();
                                    methods.dynamicSleep(Bank.isOpen(), 800, 1200);
                                } else {
                                    Bank.depositAll();
                                    methods.dynamicSleep(needToBank(28), 800, 1200);
                                }
                            } else if (Bank.isOpen()) {
                                Bank.close();
                                methods.dynamicSleep(!Bank.isOpen(), 800, 1200);
                            } else {
                                if (fred == null) {
                                    LogHandler.log("want to quest");
                                    methods.walkTo(sheepQuest, 3);
                                } else {
                                    if (fred != null) {
                                        if (!NPCChat.isChatOpen()) {
                                            methods.talkTo("Fred the Farmer");
                                            methods.dynamicSleep(NPCChat.isChatOpen(), 600, 800);
                                        } else if (NPCChat.canContinue()) {
                                            methods.handleContinue();
                                            methods.dynamicSleep(!NPCChat.canContinue(), 600, 800);
                                        } else if (NPCChat.isChatOptionsOpen()) {
                                            NPCChat.selectOption("I'm looking for a quest.");
                                            methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                            NPCChat.selectOption("Yes okay. I can do that.");
                                            methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                            NPCChat.selectOption("Of course!");
                                            methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                            NPCChat.selectOption("I'm something of an expert actually!");
                                            methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                        }
                                    }
                                }
                            }
                            break;
                        case 1:
                            if (Inventory.getCount("Wool") >= 20 || Inventory.getCount("Wool") + Inventory.getCount("Ball of wool") >= 20) {
                                spin = true;
                            }
                            Tile shears = new Tile(3191, 3272, 0);
                            GroundItem shears1 = GroundItems.getNearest("Shears");
                            NPC woolySheep = Npcs.getNearest(2794, 2800, 2802, 2795, 2796, 2801);
                            Tile wheel = new Tile(3209, 3213, 1);
                            GameObject spinningWheel = GameObjects.getNearest("Spinning wheel");
                            WidgetChild spinWidget = Widgets.getWidget(459, 100);
                            WidgetChild enterAmount = Widgets.getWidget(162, 32);
                            Tile sheepQuest2 = new Tile(3190, 3272, 0);
                            Tile pen = new Tile(3197, 3278, 0);
                            GameObject stile = GameObjects.getNearest("Stile");
                            NPC fred2 = Npcs.getNearest("Fred the Farmer");
                            if (!hasShears() && !timeToSpin() && !gotBalls()) {
                                if (shears.distance() > 1) {
                                    if (shears != null) {
                                        methods.walkTo(shears, 1);
                                    }
                                } else if (shears1 != null) {
                                    shears1.pickUp();
                                    methods.dynamicSleep(Inventory.contains("Shears"), 600, 800);
                                } else if (shears1 == null && shears.distance() <= 1) {
                                    sleep(1000, 2000);
                                }
                            } else if (timeToShear() && !timeToSpin() && !gotBalls()) {
                                if (sheep.distance() > 9) {
                                    methods.walkTo2(sheep, 9);
                                } else if (woolySheep != null && woolySheep.isOnScreen()) {
                                    if (Players.getLocal().getAnimation() == -1) {
                                        woolySheep.interact("Shear");
                                        sleep(2000, 3000);
                                    }
                                } else if (woolySheep != null && !woolySheep.isOnScreen()) {
                                    Camera.turnTo(woolySheep);
                                    sleep(600, 800);
                                    if (methods.doingNothing()) {
                                        Walking.walkTileMM(woolySheep.getLocation());
                                        sleep(600, 800);
                                    }
                                }
                            } else if (!timeToShear() && !gotBalls() && !timeToSpin()) {
                                if (Inventory.getCount("Wool") >= 20) {
                                    spin = true;
                                }
                            } else if (timeToSpin() && !gotBalls()) {
                                if (wheel != null && wheel.distance() > 1 && !spinWidget.isOnScreen() && methods.doingNothing() && !makeXOnScreen() && !currentlySpinning()) {
                                    methods.walkTo(wheel, 1);
                                } else {
                                    if (spinningWheel != null && !spinWidget.isOnScreen() && methods.doingNothing() && !makeXOnScreen() && !currentlySpinning()) {
                                        spinningWheel.interact("Spin");
                                        methods.dynamicSleep(spinWidget.isOnScreen(), 1000, 1200);
                                    } else if (spinningWheel != null && spinWidget.isOnScreen() && !makeXOnScreen() && !currentlySpinning()) {
                                        spinWidget.interact("Make X");
                                        methods.dynamicSleep(makeXOnScreen(), 1000, 1200);
                                    } else if (makeXOnScreen() || currentlySpinning()) {
                                        if (makeXOnScreen()) {
                                            Keyboard.sendText("" + Random.nextInt(20, 999), true);
                                        }
                                        inSpin = 1;
                                        methods.dynamicSleep(!makeXOnScreen(), 1000, 1200);
                                    }
                                }
                            } else if (gotBalls()) {
                                if (sheepQuest2.distance() > 3) {
                                    methods.walkTo(sheepQuest2, 3);
                                } else {
                                    if (fred2 != null) {
                                        if (!NPCChat.isChatOpen()) {
                                            methods.talkTo("Fred the Farmer");
                                            methods.dynamicSleep(NPCChat.isChatOpen(), 600, 800);
                                        } else if (NPCChat.canContinue()) {
                                            methods.handleContinue();
                                            methods.dynamicSleep(!NPCChat.canContinue(), 600, 800);
                                        } else if (NPCChat.isChatOptionsOpen()) {
                                            NPCChat.selectOption("I'm back!");
                                            methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                            NPCChat.selectOption("Yes okay. I can do that.");
                                            methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                            NPCChat.selectOption("Of course!");
                                            methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                            NPCChat.selectOption("I'm something of an expert actually!");
                                            methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                        }
                                    }
                                }
                            }
                            break;
                        case 20:
                            NPC fred3 = Npcs.getNearest("Fred the Farmer");
                            if (fred3 != null) {
                                if (!NPCChat.isChatOpen()) {
                                    methods.talkTo("Fred the Farmer");
                                    methods.dynamicSleep(NPCChat.isChatOpen(), 600, 800);
                                } else if (NPCChat.canContinue()) {
                                    methods.handleContinue();
                                    methods.dynamicSleep(!NPCChat.canContinue(), 600, 800);
                                } else if (NPCChat.isChatOptionsOpen()) {
                                    NPCChat.selectOption("I'm back!");
                                    methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                    NPCChat.selectOption("Yes okay. I can do that.");
                                    methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                    NPCChat.selectOption("Of course!");
                                    methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                    NPCChat.selectOption("I'm something of an expert actually!");
                                    methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                }
                            }
                            break;
                        //                Case 1 = 1 quest point
//                do sheep shearer quest
                    }
                break;

                case 2:
                    Tile juliet = new Tile(3158, 3426, 1);
                    NPC Romeo = Npcs.getNearest("Romeo");
                    NPC Juliet = Npcs.getNearest("Juliet");
                    NPC phillipa = Npcs.getNearest("Phillipa");
                    GameObject tomb = GameObjects.getNearest("Tomb");
                    switch (ROMEOSETTING) {
                        case 0:
                            if (Romeo == null) {
                                if (romeo.getCentralTile().distance() > 10) {
                                    methods.walkTo(fountain, 10);
                                    LogHandler.log("This");
                                }
                            } else if (Romeo != null && !Romeo.isOnScreen()) {
                                Camera.turnTo(Romeo);
                                methods.dynamicSleep(Romeo.isOnScreen(), 600, 800);
                                Walking.walkTileMM(Romeo.getLocation());
                                methods.dynamicSleep(Romeo.isOnScreen(), 600, 800);
                            } else if (Romeo != null && Romeo.isOnScreen()) {
                                if (!NPCChat.isChatOpen()) {
                                    methods.talkTo("Romeo");
                                    methods.dynamicSleep(NPCChat.isChatOpen(), 600, 800);
                                } else if (NPCChat.canContinue()) {
                                    methods.handleContinue();
                                    methods.dynamicSleep(!NPCChat.canContinue(), 600, 800);
                                } else if (NPCChat.isChatOptionsOpen()) {
                                    NPCChat.selectOption("Perhaps I could help to find her for you?");
                                    methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                    NPCChat.selectOption("Yes, ok, I'll let her know.");
                                    methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                    NPCChat.selectOption("Of course!");
                                    methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                    NPCChat.selectOption("I'm something of an expert actually!");
                                    methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                }
                            }
                            break;
                        case 10:
                            Tile stairs = new Tile(3159, 3436);
                            if (Juliet == null) {
                                if (juliet.distance() > 5) {
                                    WebPath path = web.findPath(juliet);
                                    if (path != null) {
                                        path.traverse();
                                        sleep(600, 800);
                                    }
                                }
                            } else if (Juliet != null && Juliet.distance() > 1) {
                                WebPath path = web.findPath(Juliet.getLocation());
                                Camera.turnTo(Juliet);
                                methods.dynamicSleep(Juliet.isOnScreen(), 600, 800);
                                if (path != null) {
                                    path.traverse();
                                    sleep(600, 800);
                                }
                            } else if (Juliet != null && Juliet.isOnScreen()) {
                                if (!NPCChat.isChatOpen()) {
                                    methods.talkTo("Juliet");
                                    methods.dynamicSleep(NPCChat.isChatOpen(), 600, 800);
                                } else if (NPCChat.canContinue()) {
                                    methods.handleContinue();
                                    methods.dynamicSleep(!NPCChat.canContinue(), 600, 800);
                                } else if (NPCChat.isChatOptionsOpen()) {
                                    NPCChat.selectOption("Perhaps I could help to find her for you?");
                                    methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                    NPCChat.selectOption("Yes, ok, I'll let her know.");
                                    methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                    NPCChat.selectOption("Of course!");
                                    methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                    NPCChat.selectOption("I'm something of an expert actually!");
                                    methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                }
                            }
                            break;
                        case 20:
                            if (Romeo == null) {
                                if (romeo.getCentralTile().distance() > 10) {
                                    if (Juliet != null) {
                                        Tile tile = new Tile(3155, 3436, 1);
                                        WebPath path = web.findPath(romeo.getCentralTile());
                                        GameObject stairs1 = GameObjects.getNearest("Staircase");
                                        if (tile != null) {
                                            if (path != null) {
                                                path.traverse();
                                                sleep(600, 800);
                                            }
                                        }
                                    } else {
                                        methods.walkTo(romeo.getCentralTile(), 10);
                                    }
                                }
                            } else if (Romeo != null && !Romeo.isOnScreen()) {
                                Camera.turnTo(Romeo);
                                methods.dynamicSleep(Romeo.isOnScreen(), 600, 800);
                                Walking.walkTileMM(Romeo.getLocation());
                                methods.dynamicSleep(Romeo.isOnScreen(), 600, 800);
                            } else if (Romeo != null && Romeo.isOnScreen()) {
                                if (!NPCChat.isChatOpen()) {
                                    methods.talkTo("Romeo");
                                    methods.dynamicSleep(NPCChat.isChatOpen(), 600, 800);
                                } else if (NPCChat.canContinue()) {
                                    methods.handleContinue();
                                    methods.dynamicSleep(!NPCChat.canContinue(), 600, 800);
                                } else if (NPCChat.isChatOptionsOpen()) {
                                    NPCChat.selectOption("Perhaps I could help to find her for you?");
                                    methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                    NPCChat.selectOption("Yes, ok, I'll let her know.");
                                    methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                    NPCChat.selectOption("Of course!");
                                    methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                    NPCChat.selectOption("I'm something of an expert actually!");
                                    methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                }
                            }
                            break;
                        case 30:
                            NPC father = Npcs.getNearest("Father Lawrence");
                            Tile fatherT = new Tile(3253, 3484, 0);
                            if (father == null) {
                                if (fatherT.distance() > 5) {
                                    methods.walkTo(fatherT, 5);
                                }
                            } else if (father != null && !father.isOnScreen()) {
                                Camera.turnTo(father);
                                methods.dynamicSleep(father.isOnScreen(), 600, 800);
                                Walking.walkTileMM(father.getLocation());
                                methods.dynamicSleep(father.isOnScreen(), 600, 800);
                            } else if (father != null && father.isOnScreen()) {
                                if (!NPCChat.isChatOpen()) {
                                    methods.talkTo("Father Lawrence");
                                    methods.dynamicSleep(NPCChat.isChatOpen(), 600, 800);
                                } else if (NPCChat.canContinue()) {
                                    methods.handleContinue();
                                    methods.dynamicSleep(!NPCChat.canContinue(), 600, 800);
                                } else if (NPCChat.isChatOptionsOpen()) {
                                    NPCChat.selectOption("Perhaps I could help to find her for you?");
                                    methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                    NPCChat.selectOption("Yes, ok, I'll let her know.");
                                    methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                    NPCChat.selectOption("Of course!");
                                    methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                    NPCChat.selectOption("I'm something of an expert actually!");
                                    methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                }
                            }
                            break;
                        case 40:
                            NPC apoth = Npcs.getNearest("Apothecary");
                            Tile apothT = new Tile(3194, 3403, 0);
                            if (apoth == null) {
                                if (apothT.distance() > 5) {
                                    methods.walkTo(apothT, 5);
                                }
                            } else if (apoth != null && !apoth.isOnScreen()) {
                                Camera.turnTo(apoth);
                                methods.dynamicSleep(apoth.isOnScreen(), 600, 800);
                                Walking.walkTileMM(apoth.getLocation());
                                methods.dynamicSleep(apoth.isOnScreen(), 600, 800);
                            } else if (apoth != null && apoth.isOnScreen()) {
                                if (!NPCChat.isChatOpen()) {
                                    methods.talkTo("Apothecary");
                                    methods.dynamicSleep(NPCChat.isChatOpen(), 600, 800);
                                } else if (NPCChat.canContinue()) {
                                    methods.handleContinue();
                                    methods.dynamicSleep(!NPCChat.canContinue(), 600, 800);
                                } else if (NPCChat.isChatOptionsOpen()) {
                                    NPCChat.selectOption("Where can I get these berries?");
                                    methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                    NPCChat.selectOption("Yes, ok, I'll let her know.");
                                    methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                    NPCChat.selectOption("Of course!");
                                    methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                    NPCChat.selectOption("I'm something of an expert actually!");
                                    methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                }
                            }
                            break;
                        case 50:
                            NPC apoth2 = Npcs.getNearest("Apothecary");
                            Tile apothT2 = new Tile(3194, 3403, 0);
                            GameObject bush = GameObjects.getNearest("Cadava bush");
                            Tile bushT = new Tile(3270, 3369, 0);
                            if (!berriesCollected() && !potionCollected()) {
                                if (bush == null || bush != null && !bush.isOnScreen() && bush.distance() > 5) {
                                    if (bushT != null) {
                                        methods.walkTo(bushT, 5);
                                    }
                                } else if (bush != null && !bush.isOnScreen()) {
                                    LogHandler.log("In here");
                                    Camera.turnTo(bush);
                                    methods.dynamicSleep(bush.isOnScreen(), 600, 800);
                                    Walking.walkTileMM(bush.getLocation());
                                    methods.dynamicSleep(bush.isOnScreen(), 600, 800);
                                } else if (bush != null && bush.isOnScreen()) {
                                    bush.interact("Pick-from");
                                    methods.dynamicSleep(berriesCollected(), 600, 800);
                                }
                            } else if (berriesCollected()) {
                                if (apoth2 == null) {
                                    if (apothT2.distance() > 5) {
                                        methods.walkTo(apothT2, 5);
                                    }
                                } else if (apoth2 != null && !apoth2.isOnScreen()) {
                                    Camera.turnTo(apoth2);
                                    methods.dynamicSleep(apoth2.isOnScreen(), 600, 800);
                                    Walking.walkTileMM(apoth2.getLocation());
                                    methods.dynamicSleep(apoth2.isOnScreen(), 600, 800);
                                } else if (apoth2 != null && apoth2.isOnScreen()) {
                                    if (!NPCChat.isChatOpen()) {
                                        methods.talkTo("Apothecary");
                                        methods.dynamicSleep(NPCChat.isChatOpen(), 600, 800);
                                    } else if (NPCChat.canContinue()) {
                                        methods.handleContinue();
                                        methods.dynamicSleep(!NPCChat.canContinue(), 600, 800);
                                    } else if (NPCChat.isChatOptionsOpen()) {
                                        NPCChat.selectOption("Where can I get these berries?");
                                        methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                        NPCChat.selectOption("Yes, ok, I'll let her know.");
                                        methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                        NPCChat.selectOption("Of course!");
                                        methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                        NPCChat.selectOption("I'm something of an expert actually!");
                                        methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                    }
                                }
                            } else if (potionCollected()) {
                                if (Juliet == null) {
                                    if (juliet.distance() > 5) {
                                        WebPath path = web.findPath(juliet);
                                        if (path != null) {
                                            path.traverse();
                                            sleep(600, 800);
                                        }
                                    }
                                } else if (Juliet != null && Juliet.distance() > 1) {
                                    WebPath path = web.findPath(Juliet.getLocation());
                                    Camera.turnTo(Juliet);
                                    methods.dynamicSleep(Juliet.isOnScreen(), 600, 800);
                                    if (path != null) {
                                        path.traverse();
                                        sleep(600, 800);
                                    }
                                } else if (Juliet != null && Juliet.isOnScreen() && phillipa.distance() > 2) {
                                    if (!NPCChat.isChatOpen()) {
                                        methods.talkTo("Juliet");
                                        methods.dynamicSleep(NPCChat.isChatOpen(), 1200, 1500);
                                    } else if (NPCChat.canContinue()) {
                                        methods.handleContinue();
                                        methods.dynamicSleep(!NPCChat.canContinue(), 1200, 1500);
                                    } else if (NPCChat.isChatOptionsOpen()) {
                                        NPCChat.selectOption("Perhaps I could help to find her for you?");
                                        methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                        NPCChat.selectOption("Yes, ok, I'll let her know.");
                                        methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                        NPCChat.selectOption("Of course!");
                                        methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                        NPCChat.selectOption("I'm something of an expert actually!");
                                        methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                    }
                                }
                            }
                            break;
                        case 60:
                            if (Romeo == null) {
                                if (romeo.getCentralTile().distance() > 10) {
                                    if (phillipa != null) {
                                        Tile tile = new Tile(3155, 3436, 1);
                                        WebPath path = web.findPath(romeo.getCentralTile());
                                        if (tile != null) {
                                            if (path != null) {
                                                path.traverse();
                                                sleep(600, 800);
                                            }
                                        }
                                    } else {
                                        methods.walkTo(romeo.getCentralTile(), 10);
                                    }
                                }
                            } else if (Romeo != null && !Romeo.isOnScreen()) {
                                Camera.turnTo(Romeo);
                                methods.dynamicSleep(Romeo.isOnScreen(), 600, 800);
                                Walking.walkTileMM(Romeo.getLocation());
                                methods.dynamicSleep(Romeo.isOnScreen(), 600, 800);
                            } else if (Romeo != null && Romeo.isOnScreen() && tomb == null) {
                                if (!NPCChat.isChatOpen()) {
                                    methods.talkTo("Romeo");
                                    methods.dynamicSleep(NPCChat.isChatOpen(), 600, 800);
                                } else if (NPCChat.canContinue()) {
                                    methods.handleContinue();
                                    methods.dynamicSleep(!NPCChat.canContinue(), 600, 800);
                                } else if (NPCChat.isChatOptionsOpen()) {
                                    NPCChat.selectOption("Perhaps I could help to find her for you?");
                                    methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                    NPCChat.selectOption("Yes, ok, I'll let her know.");
                                    methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                    NPCChat.selectOption("Of course!");
                                    methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                    NPCChat.selectOption("I'm something of an expert actually!");
                                    methods.dynamicSleep(!NPCChat.isChatOptionsOpen(), 600, 800);
                                }
                            }
                            break;
                    }

                    //DO ROMEO AND JULIET
                    break;
                default:
                    if (sevenQPDONE()) {
                        LogHandler.log("Quests completed, 7 QP achieved!");

                        TBot.getBot().getScriptHandler().stopScript();
                    } else {
                        LogHandler.log("Case not accounted for in code" + QPSETTING);
                    }
                    break;
            }
        }
        return 120;
    }

    public boolean sevenQPDONE (){
        return Settings.get(101) >= 7;
    }

    public boolean wheatCollected(){
        return Inventory.contains("Grain");
    }

    public boolean hopGrain(){
        return graininHop == 1;
    }
    public boolean timeToSpin(){
        return Inventory.getCount("Wool") >= 20 || Inventory.getCount("Wool") + Inventory.getCount("Ball of wool") >= 20;
    }

    public boolean makeXOnScreen(){
        WidgetChild makex = Widgets.getWidgetByText("Enter amount:");
        if(makex != null){
            return makex.isOnScreen();
        }return false;
    }

    public boolean eggCollected(){
        return Inventory.contains("Egg");
    }
    public boolean insidePen(){
        return sheep.distance() < 9 || stileClimbed == 1;
    }


    public boolean hopperFlour(){
        return Settings.get(695) == 1;
    }

    public boolean needToBank(int emptySlots){
        return Inventory.getEmptySlots() != emptySlots;
    }

    public boolean potCollected(){
        return Inventory.contains("Pot");
    }
    public boolean gotBalls(){
        return Inventory.getCount("Ball of wool") >= 20;
    }
    public boolean currentlySpinning(){
        return inSpin == 1;
    }
    public boolean bucketCollected(){
        return Inventory.contains("Bucket");
    }

    public boolean timeToShear(){
        return hasShears() && Inventory.getCount("Wool") < 20;
    }

    public boolean berriesCollected(){
        return Inventory.contains("Cadava berries");
    }

    public boolean potionCollected(){
        return Inventory.contains("Cadava potion");
    }

    public boolean milkCollected(){
        return Inventory.contains("Bucket of milk");
    }

    public void Antiban(){

if (t.getMintuesElapsed() > t2 && !Bank.isOpen() && !GrandExchange.isOpen()){
        int anti = Random.nextInt(1, 12);
        switch (anti) {

        case 1:
        Antiban.loseFocus();
        Time.sleep(Random.nextInt(2000, 4200));
        Antiban.requestFocus();
        break;

//        case 2: Antiban.hoverRandomSkill();
//        break;
//
//        case 3: Antiban.hoverRandomSkill();
//        break;

        case 2: Antiban.moveMouseInventory();
        break;

        case 3: Antiban.moveMouseInventory();
        break;

        case 4: Antiban.moveMouseInventory();
        break;

        case 5: Antiban.moveMouseRandomly();
        break;

        case 6: Antiban.moveMouseRandomly();
        break;

//        case 11: Antiban.hoverRandomSkill();
//        break;

        case 7:
        Antiban.loseFocus();
        Time.sleep(Random.nextInt(2000, 4200));
        Antiban.requestFocus();
        break;

        case 8:
        Antiban.loseFocus();
        Time.sleep(Random.nextInt(2000, 4200));
        Antiban.requestFocus();
        break;

        case 9: Antiban.moveMouseRandomly();
        break;

        case 10: Widgets.openTab(2);
        break;

default:
    t2  = t2 + Random.nextInt(5, 15);
    LogHandler.log("t2: "+ t2);
        break;
        }
        }
    }
    public void connectIRC() throws Exception{
        bc = new BotControlConnection("84.200.230.230", 6667, "Myname101");

        bc.setChannelName("#AaronC");
        bc.connect();
        bc.joinChannel();
        if (!bc.isClosed()) {
            bc.joinChannel();
        }
    }

    public boolean hasShears(){
        return Inventory.contains("Shears");
    }
    public boolean atMill(){
        Tile mill = new Tile(3167,3305,0);
        Tile mill1 = new Tile(3167,3305,1);
        Tile mill2 = new Tile(3165,3307,2);
        return mill.distance() < 5 || mill1.distance() < 5 || mill2.distance() < 5;
    }

    public boolean flourCollected(){
        return Inventory.contains("Pot of flour");
    }

    public boolean CADONE(){
        return Settings.get(29) >= 2;
    }

}
