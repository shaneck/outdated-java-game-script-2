package sevenQP;

import org.tbot.bot.TBot;
import org.tbot.internal.AbstractScript;
import org.tbot.internal.Manifest;
import org.tbot.internal.event.listeners.PaintListener;
import org.tbot.internal.handlers.LogHandler;
import org.tbot.methods.Game;
import org.tbot.methods.Random;
import org.tbot.methods.Time;
import org.tbot.util.botcontrol.BotControlConnection;
import org.tbot.util.botcontrol.BotControlMessageEvent;
import org.tbot.util.botcontrol.BotControlMessageListener;

import java.awt.*;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by jorda_000 on 2015-04-06.
 */
@Manifest(name = "GAYNESS", authors = "JJordan")
public class Main extends AbstractScript implements PaintListener, BotControlMessageListener {

    public BotControlConnection connection;

    public Main() {
        this.connection = new BotControlConnection("84.200.230.230" , this.getStart(TBot.getBot().getCurrentAccount().getUsername()));
        try {
            this.connection.connect();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.connection.setChannelName("#Bots");
        try {
            this.connection.connect();
            if (!this.connection.isClosed()) {
                this.connection.joinChannel();
                this.connection.addListener(this);
                this.connection.startListening();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onStart() {
        Main main = new Main();
        return true;
    }

    @Override
    public void onFinish() {
        try {
            this.connection.removeListener(this);
            while (!this.connection.isClosed()) {
                this.connection.close();
                Time.sleep(500);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int loop() {

        return Random.nextInt(100, 250);
    }

    @Override
    public void messageReceived(BotControlMessageEvent bcme) {
        LogHandler.log(bcme.getMessage());
        if (bcme.getMessage().equals("LOGOUT")) {
            Game.logout();
        }
    }

    @Override
    public void onRepaint(Graphics g) {
    }

    private String getStart(final String name) {
        Pattern pattern = Pattern.compile("^[^\\@]*");
        Matcher matcher = pattern.matcher(name);
        if (matcher.find()) {
            return matcher.group();
        }
        return null;
    }
}
